<?php

$fin = 'traf';

$fmtime = filemtime($fin);

$raw = json_decode(file_get_contents($fin), $assoc = true);

$routes = $raw['routes'];

$data = array();

for ($i = 0; $i < sizeOf($routes); $i++) {
	$name = $routes[$i]['summary'];
	$time = $routes[$i]['legs'][0]['duration']['text'];

	$data[$name]['time'] = $time;

	if ($i == 0) {
		$min = intval($time);
	}
	else if (intval($time) < $min) {
		$min = intval($time);
	}
}

foreach ($data as $key => $value) {
	if (intval($value['time']) == $min) {
		$data[$key]['class'] = ' class="card highlight"';
	}
	else {
		$data[$key]['class'] = ' class="card"';
	}
}

?>
