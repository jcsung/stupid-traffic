<?php

require_once 'commute.php';
require_once 'weather.php';

?>
<!DOCTYPE html>
<html>
<head>
<title>Commute</title>
<style>
body {
	font-family: Sans-serif;
	font-size: 20px;
	font-weight: bold;
}

.container {
	align-items: center;
	display: flex;
	flex-flow: row nowrap;
	justify-content: space-between;
	margin-bottom: 30px;
	width: 100%;
}

.centered {
	justify-content: center !important;
}

.card {
	align-items: center;
	border: 1px solid #000000;
	display: flex;
	flex-flow: column nowrap;
	justify-content: space-between;
	padding: 4px;
}

.time {
	font-size: 16px;
}

.highlight {
	background-color: #ADD8E6;
	border: 3px solid #007FFF !important;
}

.extrema div {
	width: 45%;
}

.borderless {
	border: 0 !important;
}
</style>

<meta http-equiv="refresh"content="900;url=."/>
</head>
<body>
<div class="container centered"><div>As of <?php echo date("F d, Y, H:i", $fmtime); ?></div></div>
<div class="container">
<?php
$width = sizeOf($data) > 0 ? intval(intval(100 / sizeOf($data)) / 10) * 10 : 0;

$width = 'width: ' . $width . ($width != 0 ? '%' : '');

foreach ($data as $name => $time) {
	echo "<div". $time['class'] . " style=\"" . $width . "\"><div class=\"name\">$name</div><div class=\"time\">" . $time['time'] . "</div></div>\n";
}
?>
</div>

<div class="container extrema">
	<div class="borderless card low">Low: <?php echo $loT_display; ?></div>
	<div class="borderless card high">High: <?php echo $hiT_display; ?></div>
</div>
	
<div class="container weather">
	<?php
		$weather_width = sizeOf($weather_types) > 0 ? intval(intval(100 / sizeOf($weather_types)) / 10) * 10 : 0;
		$weather_width = 'width: ' . $weather_width . ($weather_width != 0 ? '%': '');

		for ($i = 0; $i < sizeOf($weather_types); $i++) {
			echo '<div class="borderless card" style="' . $weather_width . '">';
			echo '<div class="icon"><img src="' . $weather_types[$i]['icon'] . '" alt="" border="0" /></div>';
			echo '<div class="alt">' . $weather_types[$i]['icon_alt'] . '</div>';
			echo '<div class="times">' . $weather_types[$i]['times'] . '</div>';
			echo '</div>';
		}
	?>	
</div>
</body>
</html>
