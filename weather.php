<?php

$fin = 'hourly';
//date_default_timezone_set('UTC');
$range = 24 * 3600; // 24 hours' worth

$now = mktime();

$raw = json_decode(file_get_contents($fin));

$hourly_raw = $raw->properties->periods;

$lo = $hourly_raw[0]->temperature;
$hi = $hourly_raw[0]->temperature;

$un = $hourly_raw[0]->temperatureUnit;

$hash = array();

for ($i = 0; $i < sizeOf($hourly_raw); $i++) {
	$cur = $hourly_raw[$i];

	$startTime = $cur->startTime;
	$cur_time = strtotime($startTime);

	if ($cur_time < $now) {
		continue;
	}

	if ($cur_time >= $now + $range) {
		break;
	}

	if ($lo > $cur->temperature) {
		$lo = $cur->temperature;
	}

	if ($hi < $cur-> temperature) {
		$hi = $cur->temperature;
	}

	$icon = $cur->icon;

	if (!array_key_exists($icon, $hash)) {
		$hash[$icon] = array();
	}

	array_push($hash[$icon], date("H", $cur_time));
}

$weather_types = array();

$keys = array_keys($hash);

for ($i = 0; $i < sizeOf($keys); $i++) {
	$curKey = $keys[$i];
	$curArr = $hash[$curKey];

	$curStr = $curArr[0];

	$prev = $curStr;
	for ($j = 1; $j < sizeOf($curArr); $j++) {
		if ($curArr[$j] != $prev + 1) {
			$curStr .= ' - ' . $prev . ', ' . $curArr[$j];
		}

		$prev = $curArr[$j];
	}

	if (sizeOf($curArr) > 2) {
		if ($prev == $curArr[$j - 2] + 1) {
			$curStr .= ' - ' . $prev;
		}
		else {
			$curStr .= ', ' . $prev;
		}
	}

	// echo "$curStr\n";

	$curVal = array();
	$curVal['icon'] = $curKey;
	$temp = explode('/', $curKey);
	$curVal['icon_alt'] = strtoupper(str_replace('_', ' ', str_replace('?size=small', '', $temp[sizeOf($temp) - 1])));
	$curVal['times'] = $curStr;

	array_push($weather_types, $curVal);
}


$loT_display = "$lo&deg;$un";
$hiT_display = "$hi&deg;$un";

//echo print_r($hourly_raw, true)."\n";

//echo print_r($weather_types, true)."\n";
?>
